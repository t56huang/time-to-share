//
//  FirstViewController.swift
//  TimeToShare
//
//  Created by Taoming Huang on 2018-06-06.
//  Copyright © 2018 Taoming Huang. All rights reserved.
//

import UIKit
import FSCalendar
import Eureka
import Firebase
import FirebaseFirestore
import FirebaseAuth
import CoreLocation
import SwiftSpinner
import ISTimeline
import SwiftIconFont

class CalendarController: UIViewController, FSCalendarDataSource, FSCalendarDelegate, UITableViewDelegate, UITableViewDataSource, CreateEventControllerDelegate, EditEventControllerDelegate, UIGestureRecognizerDelegate {

    // Firebase related variables
    var db: Firestore!
    var uid: String!
    var userName : String!
    var monthlyview = true

    @IBOutlet weak var timeline: ISTimeline!
    @IBOutlet weak var eventListTableView: UITableView!
    @IBOutlet weak var calendar: FSCalendar!
    @IBOutlet weak var calendarHeightConstraint: NSLayoutConstraint!

    fileprivate let gregorian = Calendar(identifier: .gregorian)
    fileprivate let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss Z"
        return formatter
    }()

    fileprivate lazy var scopeGesture: UIPanGestureRecognizer = {
        [unowned self] in
        let panGesture = UIPanGestureRecognizer(target: self.calendar, action: #selector(self.calendar.handleScopeGesture(_:)))
        panGesture.delegate = self
        panGesture.minimumNumberOfTouches = 1
        panGesture.maximumNumberOfTouches = 2
        return panGesture
    }()

    // Collection of events
    fileprivate var eventList = [String:[String: Any]]()
    // Collection of events being displayed. If referring a cell
    // that is displayed, please use this one.
    fileprivate var eventDisplayList = [[String: Any]]()
    fileprivate var eventDisplayKeyList = [String]()
    fileprivate var eventTimelineList = [[String: Any]]()
    
    // Save vars
    fileprivate var eventIDExpectedToSave = String()
    
    // !!!!Delete vars MARK: not sure if this is the appropriate way to initialize a var
    fileprivate var deleteEventIndexPath = IndexPath()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadingSpinner()

        // Firebase related settings
        let settings = FirestoreSettings()
        settings.isPersistenceEnabled = true
        settings.areTimestampsInSnapshotsEnabled = true
        Firestore.firestore().settings = settings
        uid = (Auth.auth().currentUser?.uid)
        db = Firestore.firestore()
        db.collection("users").document(uid).getDocument(){ (document, err) in
            if (err != nil) {
                print("Error retreiving snapshots \(err!)")
                return
            }
            
            self.userName = document?.data()!["username"] as! String
            print("username is \(self.userName)")
        }

        self.getJoinedEvents()
        self.listenForUserEvents()

        eventListTableView.delegate = self
        eventListTableView.dataSource = self

        self.view.addGestureRecognizer(self.scopeGesture)
        self.eventListTableView.panGestureRecognizer.require(toFail: self.scopeGesture)
        self.calendar.scope = .month

        self.calendar.appearance.headerMinimumDissolvedAlpha = 0.0;
        
        timeline.isHidden = true
        timeline.bubbleColor = UIColor(red: 240/255, green: 240/255, blue: 248/255, alpha: 1)
        timeline.descriptionColor = .black
        timeline.titleColor = UIColor(red: 0/255, green: 122/255, blue: 255/255, alpha: 1)
        timeline.pointDiameter = 12.0
        timeline.lineWidth = 4.0
        timeline.bubbleRadius = 6.0
        timeline.bubbleArrows = false

        /*
        let dates = [
            self.gregorian.date(byAdding: .day, value: 0, to: Date())
        ]
        dates.forEach { (date) in
            self.calendar.select(date, scrollToDate: false)
        }*/
    }
    func loadingSpinner() {
        SwiftSpinner.show(duration: 1.0, title: "Loading...")
    }

    // MARK:- UIGestureRecognizerDelegate
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        let shouldBegin = self.eventListTableView.contentOffset.y <= -self.eventListTableView.contentInset.top
        if shouldBegin {
            let velocity = self.scopeGesture.velocity(in: self.view)
            switch self.calendar.scope {
            case .month:
                return velocity.y < 0
            case .week:
                return velocity.y > 0
            }
        }
        return shouldBegin
    }

    func saveTapped(value: [String: Any]) {
        var ref: DocumentReference? = nil
        let friendlist: [String] = value["friends"] as! [String]
        ref = db.collection("events").addDocument(data: value.merging(["created-by": self.uid,"username": self.userName]) { $1 }) { err in
            if let err = err {
                print("Error adding document: \(err)")
            } else {
//                print("event added with ID: \(ref!.documentID)")

                // add documentID to joinedEventsIDs if the event is public
                if let publicEvent = ((value["public"]) as! String).toBool(), publicEvent {
                    let newJoinedEventsIDs = self.joinedEventsIDs + [ref!.documentID]
                    self.db.collection("users").document(self.uid).updateData([
                        "joinedEvents": newJoinedEventsIDs
                    ]) { err in
                        if let err = err {
                            print("addJoinedEvent - Error updating document: \(err)")
                        } else {
//                        print("addJoinedEvent - Document successfully updated")
                        }
                    }
                }
                
                for friendid in friendlist {
                    self.db.collection("users").document(friendid).getDocument { (document, error) in
                        if let document = document, document.exists {
                            //let dataDescription = document.data().map(String.init(describing:)) ?? "nil"
                            
                            var inboxlist : [[String: String]]
                            if document.data()!["inbox"] != nil {
                                inboxlist = document.data()!["inbox"] as! [[String: String]]
                            }
                            else {
                                inboxlist = [[String: String]]()
                            }
                            
                            var inbox = [String: String]()
                            inbox["invite"] = "true"
                            inbox["name"] = self.userName
                            inbox["eventname"] = value["title"] as? String
                            inbox["eventID"] = ref!.documentID
                            inboxlist.append(inbox)
                            self.db.collection("users").document(friendid).updateData([
                                "inbox": inboxlist,
                                "inboxupdated": true
                            ]) { err in
                                if let err = err {
                                    print("Error updating friend's inbox: \(err)")
                                } else {
                                    print("Friend's inbox successfully updated")
                                }
                            }

                            
                        } else {
                            print("User does not exist")
                        }
                    }
                }
            }
        }
    }

    var joinedEventsIDs = [String]()
    var joinedEventsList = [String:[String: Any]]()
    var userEventsList = [String:[String: Any]]()

    func getJoinedEvents() {
        db.collection("users").document(self.uid)
            .addSnapshotListener { documentSnapshot, error in
                guard let document = documentSnapshot else {
                    print("Error fetching document: \(error!)")
                    return
                }

                if let joinedEventsIDs = document.data()?["joinedEvents"] as? Array<String> {
                    // add new values to array
                    if joinedEventsIDs.unique(from: self.joinedEventsIDs).count > 0 {
                        self.joinedEventsIDs = joinedEventsIDs
                        self.listenForJoinedEvents()
                    }

                    // remove old values and remove entry from joinedEventsList
                    
                    let removedEventsIDs = self.joinedEventsIDs.unique(from: joinedEventsIDs)
                    print("removed EventsID is \(removedEventsIDs.count)")
                    if removedEventsIDs.count > 0 {
                        for eventID in removedEventsIDs {
                            self.joinedEventsList.removeValue(forKey: eventID)
                            
                            // remove events from table view display list if any
                            if let indexOfEvent = self.eventDisplayKeyList.index(of: eventID) {
                                self.eventDisplayKeyList.remove(at: indexOfEvent)
                                self.eventDisplayList.remove(at: indexOfEvent)
                            }
                        }

                        self.joinedEventsIDs = joinedEventsIDs
                        print("before \(self.eventList.count)")
                        self.eventList = self.userEventsList.merging(self.joinedEventsList) { $1 }
                        print("after \(self.eventList.count)")
                        self.updateTimeline()

                        self.calendar.reloadData()
                        self.eventListTableView.reloadData()
                    }
                }
        }
    }

    private func listenForJoinedEvents() {
        db.collection("events").addSnapshotListener { querySnapshot, error in
            guard let snapshot = querySnapshot else {
                print("Error retreiving snapshots \(error!)")
                return
            }

            snapshot.documentChanges.forEach { diff in
                if self.joinedEventsIDs.contains(diff.document.documentID) {
                    if (diff.type == .added || diff.type == .modified) {
                        self.joinedEventsList[diff.document.documentID] = diff.document.data()
                    }
                }
            }

            // merge by overriding userEventsList
            self.eventList = self.userEventsList.merging(self.joinedEventsList) { $1 }
            self.updateTimeline()
//            print("joined count: \(self.joinedEventsList.count)")
            
            // add events to table view display list if any
            if let selectedDate = self.calendar.selectedDate {
                self.updateDisplayListWithDate(selectedDate)
            }

            self.calendar.reloadData()
            self.eventListTableView.reloadData()
        }
    }

    private func listenForUserEvents() {
        db.collection("events").whereField("created-by", isEqualTo: self.uid)
            .addSnapshotListener { querySnapshot, error in
                guard let snapshot = querySnapshot else {
                    print("Error retreiving snapshots \(error!)")
                    return
                }

                //print("snapshot result is \(snapshot.getDocument())")
                snapshot.documentChanges.forEach { diff in
                    if (diff.type == .added || diff.type == .modified) {
                        self.userEventsList[diff.document.documentID] = diff.document.data()
                    }

                    if (diff.type == .removed) {
                        self.userEventsList.removeValue(forKey: diff.document.documentID)
                    }
                }

                // merge by overriding joinedEventsList
                self.eventList = self.joinedEventsList.merging(self.userEventsList) { $1 }
                self.updateTimeline()
//                print("user events count: \(self.userEventsList.count)")

                // initial view that there is no selected date, show events for today
                if let selectedDate = self.calendar.selectedDate {
                    self.updateDisplayListWithDate(selectedDate)
                } else {
                    self.updateDisplayListWithDate(Date())
                }

                self.calendar.reloadData()
                self.eventListTableView.reloadData()
            }
    }

    func updateDisplayListWithDate(_ date: Date) {
        eventDisplayList.removeAll()
        eventDisplayKeyList.removeAll()
        for (eventID, event) in self.eventList {
            let string = event["starts"] as! String
            let date2 = self.formatter.date(from: string)
            if self.onSameDay(date, date2!) {
                self.eventDisplayList.append(event)
                self.eventDisplayKeyList.append(eventID)
            }
        }
    }

    // MARK: FSCalendarDelegate

    func calendar(_ calendar: FSCalendar, boundingRectWillChange bounds: CGRect, animated: Bool) {
        self.calendarHeightConstraint.constant = bounds.height
        self.view.layoutIfNeeded()
    }

    // Triggers when date selected
    func calendar(_ calendar: FSCalendar, didSelect date: Date) {
        eventDisplayList.removeAll()
        eventDisplayKeyList.removeAll()
        updateDisplayListWithDate(date)
        eventListTableView.reloadData()
    }

    // FSCalendarDataSource
    // Determines if the date has event
    func calendar(_ calendar: FSCalendar, hasEventFor date: Date) -> Bool {
        for (_, event) in eventList {
            let starts = event["starts"] as! String
            let eventDate = self.formatter.date(from: starts)
            if onSameDay(date, eventDate!) {
                return true
            }
        }
        return false
    }

    // MARK: TableView delegate
    // return the number of count in the display list
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (eventDisplayList.count);
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "EventCell", for: indexPath)
        cell.textLabel?.text = eventDisplayList[indexPath.row]["title"] as? String
        return (cell)
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            deleteEventIndexPath = indexPath
            confirmDelete(indexPath: deleteEventIndexPath)
        }
    }

    // MARK: select the table cell
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "showEventDetailSegue", sender: eventListTableView.cellForRow(at: indexPath))
    }

    // MARK: TableView row action
    func confirmDelete(indexPath: IndexPath) {
        if let deleteMessage = eventDisplayList[indexPath.row]["title"] as? String{
            let alert = UIAlertController(title: "Delete Event", message: "Are you sure you want to permanently delete \(deleteMessage)?", preferredStyle: .actionSheet)
            let DeleteAction = UIAlertAction(title: "Delete", style: .destructive, handler: handleDeleteEvent)
            let CancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: cancelDeleteEvent)
            
            alert.addAction(DeleteAction)
            alert.addAction(CancelAction)
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func handleDeleteEvent(alertAction: UIAlertAction!) -> Void {
        self.eventListTableView.beginUpdates()
        
        
        // Delete the event in the firebase
        let deleteEventID = eventDisplayKeyList[deleteEventIndexPath.row]
        
        // Check if this event is a joined event, if so, delete it from
        // joined list instead of delete from event collection
        if joinedEventsIDs.contains(deleteEventID) {
            let removedJointsIDs = self.joinedEventsIDs.filter{ $0 != deleteEventID }
            db.collection("users").document((self.uid)!).updateData(["joinedEvents": removedJointsIDs]){ err in
                if let err = err {
                    print("removeJoinedEvent - Error updating document: \(err)")
                } else {
                    //                print("removeJoinedEvent - Document successfully updated")
                }
            }
            self.getJoinedEvents()
        }else{
            db.collection("events").document(deleteEventID).delete() { err in
                if let err = err {
                    print("Error removing document: \(err)")
                } else {
                    print("Document with \(deleteEventID) successfully removed!")
                }
            }
        }
        
        // delete the event from eventlist, eventDisplaylist
        self.eventDisplayKeyList.remove(at: deleteEventIndexPath.row)
        self.eventDisplayList.remove(at: deleteEventIndexPath.row)
        self.eventList.removeValue(forKey: deleteEventID)
        self.updateTimeline()
        
        // update the table view cell
        self.eventListTableView.deleteRows(at: [deleteEventIndexPath], with: .automatic)
        //deleteEventIndexPath = nil
        self.eventListTableView.endUpdates()
    }
    
    func cancelDeleteEvent(alertAction: UIAlertAction!) {
        //deleteEventIndexPath = nil
    }

    // the delegate function in editEventController
    func updateEvent(value: [String : String]) {
        db.collection("events").document(self.eventIDExpectedToSave).setData(value,merge:true) { err in
            if let err = err {
                print("Error adding document: \(err)")
            } else {
                print("Document overwritten with ID: \(self.eventIDExpectedToSave)")
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showEventDetailSegue" {
            if self.monthlyview {
                print("cell selected")
                let cell = sender as! UITableViewCell
                if let indexPath = eventListTableView.indexPath(for: cell) {
                    let dest = segue.destination as! UINavigationController
                    let editEventController = dest.topViewController as! EditEventController
                    let formValue = eventDisplayList[indexPath.row]
                    eventIDExpectedToSave = eventDisplayKeyList[indexPath.row]
                    editEventController.formValue = formValue
                    eventListTableView.deselectRow(at: indexPath, animated: true)
                    editEventController.delegate = self
                }
            } else {
                let dest = segue.destination as! UINavigationController
                let editEventController = dest.topViewController as! EditEventController
                let formValue = sender as! [String: Any]
                eventIDExpectedToSave = formValue["ID"] as! String
                editEventController.formValue = formValue
                editEventController.delegate = self
            }
        }
        else {
            let dest = segue.destination as! UINavigationController
            let createEventController = dest.topViewController as! CreateEventController
            if let test = self.calendar.selectedDate {
                createEventController.selectedDate = test
            } else {
                createEventController.selectedDate = Date()
            }
            createEventController.delegate = self
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func onSameDay(_ date: Date, _ date2: Date) -> Bool {
        let calendar = Calendar.current
        let year = calendar.component(.year, from: date)
        let month = calendar.component(.month, from: date)
        let day = calendar.component(.day, from: date)
        let year2 = calendar.component(.year, from: date2)
        let month2 = calendar.component(.month, from: date2)
        let day2 = calendar.component(.day, from: date2)
        if year == year2 && month == month2 && day == day2 {
            return true
        } else {
            return false
        }
    }

    @IBAction func SwitchView(_ sender: Any) {
        if monthlyview {
            if let sender = sender as? UIBarButtonItem {
                sender.icon(from: .ionicon, code: "ios-calendar", ofSize: 25)
            }
            monthlyview = false
            timeline.isHidden = false
            calendar.isHidden = true
            eventListTableView.isHidden = true

        } else {
            if let sender = sender as? UIBarButtonItem {
                sender.icon(from: .ionicon, code: "ios-list-box", ofSize: 25)
            }
            monthlyview = true
            timeline.isHidden = true
            calendar.isHidden = false
            eventListTableView.isHidden = false
        }
    }

    private func updateTimeline() {
        var points = [ISPoint]()
        self.eventTimelineList.removeAll()
        let timelineEvents = Array(self.eventList.values)
        for var (i, data) in timelineEvents.enumerated() {
            data["ID"] = Array(self.eventList.keys)[i]
            self.eventTimelineList.append(data)
        }
        self.eventTimelineList.sort(by: {(self.formatter.date(from: $0["starts"] as! String))! < (self.formatter.date(from: $1["starts"] as! String))!})
        for data in self.eventTimelineList {
            let date = formatter.date(from: data["starts"] as! String)
            let dateTimeString = "\((date?.monthFull)!) \((date?.dayMedium)!) \((date?.year)!), \((date?.hour12)!):\((date?.minute0x)!) \((date?.amPM)!)"
            let point = ISPoint(title: dateTimeString)

            point.description = data["title"] as? String
            point.touchUpInside = {(point:ISPoint) in
                print("Clicked " + point.title)
                self.performSegue(withIdentifier: "showEventDetailSegue", sender: data)
            }
            point.pointColor = self.view.tintColor
            points.append(point)
        }
        timeline.points = points
    }
}

extension String {
    func toBool() -> Bool? {
        switch self {
        case "True", "true", "yes", "1":
            return true
        case "False", "false", "no", "0":
            return false
        default:
            return nil
        }
    }
}

extension Array where Element: Hashable {
    func unique(from other: [Element]) -> [Element] {
        let thisSet = Set(self)
        let otherSet = Set(other)
        return Array(thisSet.symmetricDifference(otherSet)).filter(self.contains)
    }
}
