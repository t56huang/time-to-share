//
//  Event.swift
//  TimeToShare
//
//  Created by Taoming Huang on 2018-07-01.
//  Copyright © 2018 Taoming Huang. All rights reserved.
//

import Foundation
import CoreLocation

class Event {
    var documentID: String
    var title: String
    var lat: Double?
    var long: Double?
    var locationName: String
    var city: String
    var username: String
    var detail: String
    var dateString: String

    var location: CLLocation?

    fileprivate let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss Z"
        return formatter
    }()

    init(dictionary: [String: AnyObject]) {
        documentID = dictionary["documentID"] as! String
        title = dictionary["title"] as? String ?? ""
        lat = dictionary["location"]!["lat"] as? Double
        long = dictionary["location"]!["long"] as? Double
        locationName = dictionary["location"]!["name"] as? String ?? ""
        city = dictionary["location"]!["city"] as? String ?? ""
        username = dictionary["username"] as? String ?? ""
        detail = dictionary["notes"] as? String ?? ""
        dateString = dictionary["starts"] as? String ?? ""

        if let lat = self.lat, let long = self.long {
            location = CLLocation(latitude: lat, longitude: long)
        }
    }

    func containsText(_ text: String) -> Bool {
        return title.lowercased().contains(text)
            || locationName.lowercased().contains(text)
            || city.lowercased().contains(text)
            || username.lowercased().contains(text)
            || detail.lowercased().contains(text)
            || (time?.lowercased().contains(text))!
            || (monthFull?.lowercased().contains(text))!
            || (date?.lowercased().contains(text))!
    }

    var time: String? {
        if let date = formatter.date(from: dateString) {
            return "\(date.hour12):\(date.minute0x) \(date.amPM)"
        } else {
            return nil
        }
    }

    var month: String? {
        if let date = formatter.date(from: dateString) {
            return "\(date.monthMedium.uppercased())"
        } else {
            return nil
        }
    }

    var monthFull: String? {
        if let date = formatter.date(from: dateString) {
            return "\(date.monthFull)"
        } else {
            return nil
        }
    }

    var date: String? {
        if let date = formatter.date(from: dateString) {
            return date.dayMedium
        } else {
            return nil
        }
    }
}

extension Formatter {
    static let monthMedium: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "LLL"
        return formatter
    }()

    static let monthFull: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "LLLL"
        return formatter
    }()

    static let dayMedium: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd"
        return formatter
    }()

    static let hour12: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "h"
        return formatter
    }()

    static let minute0x: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "mm"
        return formatter
    }()

    static let amPM: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "a"
        return formatter
    }()

    static let year: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "YYYY"
        return formatter
    }()
}

extension Date {
    var monthMedium: String  { return Formatter.monthMedium.string(from: self) }
    var monthFull: String { return Formatter.monthFull.string(from: self) }
    var dayMedium: String {return Formatter.dayMedium.string(from: self) }
    var hour12:  String      { return Formatter.hour12.string(from: self) }
    var minute0x: String     { return Formatter.minute0x.string(from: self) }
    var amPM: String         { return Formatter.amPM.string(from: self) }
    var year: String         { return Formatter.year.string(from: self) }
}
