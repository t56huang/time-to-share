//
//  CheckableTableviewCell.swift
//  TimeToShare
//
//  Created by Qiaoge Zheng on 2018-07-18.
//  Copyright © 2018 Taoming Huang. All rights reserved.
//

import Foundation
import UIKit

class CheckableTableViewCell: UITableViewCell {
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .none
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        accessoryType = selected ? UITableViewCellAccessoryType.checkmark : UITableViewCellAccessoryType.none
    }
}
