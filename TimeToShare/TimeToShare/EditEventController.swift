//
//  EditEventController.swift
//  TimeToShare
//
//  Created by Qiaoge Zheng on 2018-06-29.
//  Copyright © 2018 Taoming Huang. All rights reserved.
//

import Foundation
import UIKit
import Eureka
import Firebase
import FirebaseAuth
import CoreLocation

protocol EditEventControllerDelegate {
    func updateEvent(value: [String: String])
}

class EditEventController: FormViewController {
    
    // for saving data
    var delegate : EditEventControllerDelegate?
    var formValue = [String: Any]()
    var uid: String!
    var disable = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        uid = Auth.auth().currentUser?.uid
        
        animateScroll = true
        initializeForm()
        updateForm()

        self.navigationController?.navigationBar.topItem?.title = formValue["title"] as? String ?? "New Event"

        if let createdBy = formValue["created-by"] as? String, createdBy == self.uid && !self.disable{
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Save",
                                                                     style: .plain,
                                                                     target: self,
                                                                     action: #selector(saveTapped))
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel",
                                                                    style: .plain,
                                                                    target: self,
                                                                    action: #selector(cancelTapped))
        } else {
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Done",
                                                                    style: .plain,
                                                                    target: self,
                                                                    action: #selector(cancelTapped))
        }
    }
    
    @objc func cancelTapped(sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func saveTapped(sender: AnyObject) {
        let formValues = self.form.values()
        var dict = [String: String]()
        for (tag, value) in formValues {
            if tag == "location" {
                continue
            }
            dict[tag] = "\(value ?? "")"
        }
        self.delegate?.updateEvent(value: dict)
        self.dismiss(animated: true, completion: nil)
    }
    
    private func initializeForm() {

        form +++
            
            TextRow("title").cellSetup { cell, row in
                row.title = "Title"
                cell.textField.placeholder = row.title
            }
            
            <<< LocationRow("location").cellSetup { cell, row in
                row.title = "Location"
            }
            
            +++
            
            SwitchRow("public") {
                $0.title = "Public"
                }.onChange { [weak self] row in
                    
            }
        
            +++
        
            SwitchRow("visible") {
                $0.title = "Visible to Friends"
                }.onChange { [weak self] row in
                
            }
            
            
            +++
            
            SwitchRow("all-day") {
                $0.title = "All-Day"
                }.onChange { [weak self] row in
                    let startDate: DateTimeInlineRow! = self?.form.rowBy(tag: "starts")
                    let endDate: DateTimeInlineRow! = self?.form.rowBy(tag: "ends")
                    
                    if row.value ?? false {
                        startDate.dateFormatter?.dateStyle = .medium
                        startDate.dateFormatter?.timeStyle = .none
                        endDate.dateFormatter?.dateStyle = .medium
                        endDate.dateFormatter?.timeStyle = .none
                    }
                    else {
                        startDate.dateFormatter?.dateStyle = .short
                        startDate.dateFormatter?.timeStyle = .short
                        endDate.dateFormatter?.dateStyle = .short
                        endDate.dateFormatter?.timeStyle = .short
                    }
                    startDate.updateCell()
                    endDate.updateCell()
                    startDate.inlineRow?.updateCell()
                    endDate.inlineRow?.updateCell()
            }
            
            <<< DateTimeInlineRow("starts") {
                $0.title = "Starts"
                }
                .onChange { [weak self] row in
                    let endRow: DateTimeInlineRow! = self?.form.rowBy(tag: "ends")
                    if row.value?.compare(endRow.value!) == .orderedDescending {
                        endRow.value = Date(timeInterval: 60*60*24, since: row.value!)
                        endRow.cell!.backgroundColor = .white
                        endRow.updateCell()
                    }
                }
                .onExpandInlineRow { [weak self] cell, row, inlineRow in
                    inlineRow.cellUpdate() { cell, row in
                        let allRow: SwitchRow! = self?.form.rowBy(tag: "all-day")
                        if allRow.value ?? false {
                            cell.datePicker.datePickerMode = .date
                        }
                        else {
                            cell.datePicker.datePickerMode = .dateAndTime
                        }
                    }
                    let color = cell.detailTextLabel?.textColor
                    row.onCollapseInlineRow { cell, _, _ in
                        cell.detailTextLabel?.textColor = color
                    }
                    cell.detailTextLabel?.textColor = cell.tintColor
            }
            
            <<< DateTimeInlineRow("ends"){
                $0.title = "Ends"
                //$0.value = selectedDate.addingTimeInterval(60*60*2)
                }
                .onChange { [weak self] row in
                    let startRow: DateTimeInlineRow! = self?.form.rowBy(tag: "starts")
                    row.cell!.backgroundColor = .white
//                    if row.value?.compare(startRow.value!) == .orderedAscending {
//                        row.cell!.backgroundColor = .red
//                    }
//                    else{
//                        row.cell!.backgroundColor = .white
//                    }
                    row.updateCell()
                }
                .onExpandInlineRow { [weak self] cell, row, inlineRow in
                    inlineRow.cellUpdate { cell, dateRow in
                        let allRow: SwitchRow! = self?.form.rowBy(tag: "all-day")
                        if allRow.value ?? false {
                            cell.datePicker.datePickerMode = .date
                        }
                        else {
                            cell.datePicker.datePickerMode = .dateAndTime
                        }
                    }
                    let color = cell.detailTextLabel?.textColor
                    row.onCollapseInlineRow { cell, _, _ in
                        cell.detailTextLabel?.textColor = color
                    }
                    cell.detailTextLabel?.textColor = cell.tintColor
        }
        
        form +++
            
            PushRow<EventState>("show-as") {
                $0.title = "Show As"
                $0.options = EventState.allValues
        }
        
        form +++
            
            TextAreaRow("notes") {
                $0.placeholder = "Notes"
                $0.textAreaHeight = .dynamic(initialTextViewHeight: 50)
        }

        // disable the whole form
        if let createdBy = formValue["created-by"] as? String, createdBy != self.uid  || self.disable{
            for row in form.rows {
                row.baseCell.isUserInteractionEnabled = false
            }
        }
        if let row = form.rowBy(tag: "location") as? LocationRow {
            row.baseCell.isUserInteractionEnabled = false
        }
        if let row = form.rowBy(tag: "public") as? SwitchRow {
            row.baseCell.isUserInteractionEnabled = false
        }
        if let row = form.rowBy(tag: "visible") as? SwitchRow {
            row.baseCell.isUserInteractionEnabled = false
        }
    }
    
    enum EventState : String, CustomStringConvertible {
        case busy
        case free
        var description : String { return rawValue }
        static let allValues = [busy, free]
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func buttonTapped(cell: ButtonCellOf<String>, row: ButtonRow) {
        let inviteFriendController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "InviteFriendController")
        self.navigationController?.pushViewController(inviteFriendController, animated: true)
    }
    
    //MARK: update form row value when load view from cell
    // @ input: [String : any]
    private func updateForm() {
        if formValue.count > 0 {
            for row in formValue{
                //print(row.key, row.value)
                switch(row.key) {
                case "ends", "starts":
                    if let formRow = form.rowBy(tag: row.key) as? DateTimeInlineRow {
                        let dateString = String(describing: row.value)
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss ZZZZZ"
                        let date = dateFormatter.date(from:dateString)
                        formRow.value = date
                        formRow.reload()
                    }
                case "all-day", "public", "visible":
                    if let formRow = form.rowBy(tag: row.key) as? SwitchRow{
                        let alldayValue = row.value as? String
                        if alldayValue == "true" {
                            formRow.value = true
                        } else{
                            formRow.value = false
                        }
                        formRow.reload()
                    }else{
                        print("does not go allday case")
                    }
                case "title":
                    let formRow = self.form.rowBy(tag: row.key)
                    //print("title test", formRow?.baseValue)
                    let titleString = row.value as? String
                    formRow?.baseValue = titleString
                    //print("title test after", formRow?.baseValue)
                    formRow?.reload()
                case "show-as":
                    if let formRow = form.rowBy(tag: row.key) as? PushRow<EventState> {
                        let stateValue = EventState(rawValue: String(describing: row.value))
                        formRow.value = stateValue
                        formRow.reload()
                    }
                case "url":
                    if let formRow = form.rowBy(tag: row.key) as? URLRow {
                        let url = URL(string: String(describing: row.value))
                        formRow.value = url
                        formRow.reload()
                    }
                case "notes":
                    if let formRow = form.rowBy(tag: row.key) as? TextAreaRow {
                        formRow.value = row.value as? String
                        formRow.reload()
                    }
                case "location":
                    let formRow = self.form.rowBy(tag: row.key) as? LocationRow
                    //print("title test", formRow?.baseValue)
                    if let locationOb = row.value as? Dictionary<String, Any>{
                        let cityString = locationOb["city"] as! String
                        let streetString = locationOb["street"] as! String
                        formRow?.displayValueFor = { location in
                            return "\(streetString), \(cityString)"
                        }
                        formRow?.baseValue = locationOb
                    }else{
                        print("cast failed")
                    }
                    //print("title test after", formRow?.baseValue)
                    formRow?.reload()
                    break
                default:
                    break
                }
            }
        }
    }
}
