//
//  RegisterPageViewController.swift
//  TimeToShare
//
//  Created by Tracy on 2018-06-20.
//  Copyright © 2018 Taoming Huang. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

class RegisterPageViewController: UIViewController {
    
    //MARK: Properties
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwoodTextField: UITextField!
    @IBOutlet weak var repeatPasswoodTextField: UITextField!
    @IBOutlet weak var usernameTextField: UITextField!
    
    let db = Firestore.firestore()
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
        super.touchesBegan(touches, with: event)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Firebase related settings
        let settings = FirestoreSettings()
        settings.isPersistenceEnabled = true
        settings.areTimestampsInSnapshotsEnabled = true
        Firestore.firestore().settings = settings
        
        // Do any additional setup after loading the view.
    }
    override func viewDidLayoutSubviews() {
        addTextFieldBottmBorder(textfield: emailTextField)
        addTextFieldBottmBorder(textfield: passwoodTextField)
        addTextFieldBottmBorder(textfield: repeatPasswoodTextField)
        addTextFieldBottmBorder(textfield: usernameTextField)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Action
    @IBAction func registerButtonTapped(_ sender: Any) {
        let email = emailTextField.text
        let password = passwoodTextField.text
        let repeatPassword = repeatPasswoodTextField.text
        let username = usernameTextField.text
        
        // check the email, password and repeatPassword are not empty
        if((username?.isEmpty)! || (email?.isEmpty)! || (password?.isEmpty)! || (repeatPassword?.isEmpty)!){
            let emptyalert = UIAlertController(title: "Warnning",
                                               message: "Username, Email, Password, Repeat Password cannot be empty",
                                               preferredStyle: UIAlertControllerStyle.alert)
            emptyalert.addAction(UIAlertAction(title: "Cancel",
                                          style: UIAlertActionStyle.default,
                                          handler: nil))
            self.present(emptyalert, animated: true, completion: nil)
        }
        
        //check password matches the repeat password
        if(password != repeatPassword){
            //TODO: display alert message
            let matchalert = UIAlertController(title: "Warnning",
                                               message: "Password and Repeat Password do not match ",
                                               preferredStyle: UIAlertControllerStyle.alert)
            matchalert.addAction(UIAlertAction(title: "Cancel",
                                               style: UIAlertActionStyle.default,
                                               handler: nil))
            self.present(matchalert, animated: true, completion: nil)
            return
        }
        if((password?.count)! < 6){
            let registerAlert = UIAlertController(title: "Warning",
                                                  message: "The password must be 6 characters long or more.",
                                                  preferredStyle: UIAlertControllerStyle.alert)
            registerAlert.addAction(UIAlertAction(title: "Cancel",
                                                  style: UIAlertActionStyle.default,
                                                  handler: nil))
            self.present(registerAlert, animated: true, completion: nil)
        }
        //TODO: store data to firebase
        Auth.auth().createUser(withEmail: email!, password: password!) { (authResult, error) in
            if(error != nil){
                print(error as Any)
                return
            }
            let uid = authResult?.user.uid
            
            // Add new user with a generated ID to the users collection.
            self.db.collection("users").document(uid!).setData([
                "username": username!
            ]) { err in
                if let err = err {
                    print("Error writing document: \(err)")
                } else {
                    print("Document successfully written!")
                }
            }
            
            // Display comfirm message after sign up.
            print("Registration is successful.")
            let alert = UIAlertController(title: "TimeToShare",
                                          message: "Registration is successful.",
                                          preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK",
                                          style: UIAlertActionStyle.default){action in self.dismiss(animated: true, completion: nil)})
            
            self.present(alert, animated: true, completion: nil)
            
        }
    }
    //MARK: private function
    func addTextFieldBottmBorder(textfield: UITextField) {
        let border = CALayer()
        let width = CGFloat(1.0)
        border.borderColor = UIColor.black.cgColor
        border.frame = CGRect(x: 0,
                              y: textfield.frame.size.height - width,
                              width: textfield.frame.size.width,
                              height: textfield.frame.size.height)
        border.borderWidth = width
        textfield.layer.addSublayer(border)
        textfield.layer.masksToBounds = true
    }
    
    
}
