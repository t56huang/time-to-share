//
//  SecondViewController.swift
//  TimeToShare
//
//  Created by Taoming Huang on 2018-06-06.
//  Copyright © 2018 Taoming Huang. All rights reserved.
//

import UIKit
import Eureka
import Firebase
import FirebaseFirestore
import FirebaseAuth

class ProfileViewController: FormViewController {

    struct FormItems{
        static let name = "Username"
        static let nameLabel = "nameLabel"
        static let email = "email"
        static let emailLabel = "emailLabel"
    }
    
    let user = Auth.auth().currentUser
    let db = Firestore.firestore()
    let firebaseAuth = Auth.auth()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationController!.navigationBar.prefersLargeTitles = true
        navigationController!.navigationBar.topItem?.title = "About You"

        form +++ Section()
            <<< LabelRow(FormItems.name){ row in
                row.title = "Username:"
            }
            <<< LabelRow(FormItems.nameLabel){ row in
                let docRef = db.collection("users").document((user?.uid)!)
                docRef.getDocument { (document, error) in
                    if let document = document, document.exists {
                        let username = document.data()?["username"] as? String
                        print(username)
                        row.title = username
                        row.reload()
                    } else {
                        print("Document does not exist")
                    }
                }
                
            }
            <<< LabelRow(FormItems.email){ row in
                row.title = "Email Address:"
            }
            <<< LabelRow(FormItems.emailLabel){ row in
                row.title = user?.email
            }
        form +++ Section("")
            <<< ButtonRow { row in
                row.title = "Signout"
                }.onCellSelection{ cell, row in
                    do {
                        try self.firebaseAuth.signOut()
                        print("signout success")
                        self.dismiss(animated: true, completion: nil)
                    } catch let signOutError as NSError {
                        print ("Error signing out: %@", signOutError)
                    }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
