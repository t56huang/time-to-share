//
//  Location.swift
//  TimeToShare
//
//  Created by Qiaoge Zheng on 2018-07-15.
//  Copyright © 2018 Taoming Huang. All rights reserved.
//

import Foundation
import CoreLocation

class Location{
    var city = ""
    var lati : CLLocationDegrees
    var longi : CLLocationDegrees
    var name: String
    var street: String
    
    init() {
        name = ""
        city = "Waterloo"
        street = ""
        lati = Double()
        longi = Double()
    }
    
    func updateLocationDegree(degree: CLLocationCoordinate2D){
        lati = degree.latitude
        longi = degree.longitude
        print("Degree is updated to \(lati) \(longi) ")
    }
    
    func getFields() -> Dictionary<String ,Any>{
        var fields = [String:Any]()
        fields["name"] = self.name
        fields["city"] = self.city
        fields["lat"] = self.lati
        fields["long"] = self.longi
        fields["street"] = self.street
        return fields
    }
    
    func getUserCity(withCompletion completion: @escaping (Bool) -> Void){
        let coordinate:CLLocation = CLLocation(latitude: lati, longitude: longi)
        CLGeocoder().reverseGeocodeLocation(coordinate, completionHandler: {(placemarks, error) -> Void in
            //print(coordinate)
            
            if error != nil {
                print("Reverse geocoder failed with error" + (error?.localizedDescription)!)
                return
            }
            
            if (placemarks?.count)! > 0 {
                let pm = placemarks![0]
                print(pm.locality as Any)
                if let city = pm.locality, let street = pm.thoroughfare, let streetNumber = pm.subThoroughfare{
                    self.city = city
                    self.street = "\(streetNumber) \(street)"
                }else{
                    print("pm locality is none")
                }
                completion(true)
            }
            else {
                print("Problem with the data received from geocoder")
                completion(false)
            }
        })
    }
}
