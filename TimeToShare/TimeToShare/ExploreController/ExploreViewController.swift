//
//  ExploreViewController.swift
//  TimeToShare
//
//  Created by Taoming Huang on 2018-06-21.
//  Copyright © 2018 Taoming Huang. All rights reserved.
//

import UIKit
import Firebase
import FirebaseFirestore
import FirebaseAuth
import CoreLocation

private let reuseIdentifier = "ExploreCell"

class ExploreViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout, CLLocationManagerDelegate, ExploreViewCellDelegate {
    var db: Firestore!
    var uid: String!

    var eventLists = [Event]()
    var filteredEventLists = [Event]()
    var joinedEvents = [String]()

    let someWidthOffset: CGFloat = 2
    let someRandomMargin: CGFloat = 32

    let searchController = UISearchController(searchResultsController: nil)
    let locationManager = CLLocationManager()

    var currentLocation: CLLocation?

    let refreshControl = UIRefreshControl()

    var docCursor: DocumentSnapshot?
    var prevDocCursor: DocumentSnapshot?
    let docLimit = 3

    fileprivate let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss Z"
        return formatter
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        // configure firebase auth and db
        uid = Auth.auth().currentUser?.uid
        db = Firestore.firestore()

        // get joined event and events near user
        getJoinedEvents()
        getEvents()

        // use large titles
        navigationController!.navigationBar.prefersLargeTitles = true
        navigationController!.navigationBar.topItem?.title = "Explore"

        collectionView!.keyboardDismissMode = .onDrag
        collectionView!.alwaysBounceVertical = true

        setupLocationManager()
        setupSearchController()

        refreshControl.addTarget(self, action: #selector(refreshEvents(_:)), for: .valueChanged)
        collectionView!.refreshControl = refreshControl

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Register cell classes
        collectionView!.register(ExploreViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)
    }

    // MARK: scrol view
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let bottom: CGFloat = scrollView.contentSize.height - scrollView.frame.size.height
        let buffer: CGFloat = view.frame.height
        let scrollPosition = scrollView.contentOffset.y

        // Reached the bottom of the list
        if scrollPosition > bottom - buffer {
            infiniteScrollLoad()
        }
    }

    // MARK: get data from firestore

    func infiniteScrollLoad() {
        if let docCursor = self.docCursor {
            db.collection("events")
                .whereField("public", isEqualTo: "true")
                .order(by: "starts")
                .start(afterDocument: docCursor)
                .limit(to: docLimit)
                .getDocuments() { (querySnapshot, err) in
                    if let err = err {
                        print("Error getting documents: \(err)")
                    } else {
                        self.appendDocuments(querySnapshot)
                    }
                    self.refreshControl.endRefreshing()
            }

        }
    }

    func getEvents() {
        // initial fetch or pull to refresh
        db.collection("events")
            .whereField("public", isEqualTo: "true")
            .order(by: "starts")
            .limit(to: docLimit)
            .getDocuments() { (querySnapshot, err) in
                if let err = err {
                    print("Error getting documents: \(err)")
                } else {
                    self.eventLists.removeAll()
                    self.appendDocuments(querySnapshot)
                }
        }
    }

    func getJoinedEvents() {
        db.collection("users").document((self.uid)!)
            .addSnapshotListener { documentSnapshot, error in
                guard let document = documentSnapshot else {
                    print("Error fetching document: \(error!)")
                    return
                }
                if let joinedEvents = document.data()?["joinedEvents"] as? Array<String> {
                    self.joinedEvents.removeAll()
                    self.joinedEvents = joinedEvents
                }
                self.collectionView?.reloadData()
        }
    }

    func appendDocuments(_ querySnapshot: QuerySnapshot?) {
        for document in querySnapshot!.documents {
            let dict = document.data() as [String : AnyObject]
            let event = Event.init(dictionary: dict.merging(["documentID": document.documentID as AnyObject]) { $1 })
            if !self.eventLists.contains { $0.documentID == event.documentID } {
                self.eventLists.append(event)
            }
        }
        if (querySnapshot?.documents.count)! > 0 {
            self.docCursor = querySnapshot?.documents.last
        }
        self.collectionView?.reloadData()
    }

    // MARK: pull-to-refresh

    @objc func refreshEvents(_ sender: Any) {
        getEvents()
    }

    // MARK: ExploreViewCellDelegate

    func removeJoinedEvent(documentID: String) {
        let newJoinedEvents = joinedEvents.filter { $0 != documentID }
        db.collection("users").document((self.uid)!).updateData([
            "joinedEvents": newJoinedEvents
        ]) { err in
            if let err = err {
                print("removeJoinedEvent - Error updating document: \(err)")
            } else {
//                print("removeJoinedEvent - Document successfully updated")
            }
        }
        self.collectionView?.reloadData()
    }

    func addJoinedEvent(documentID: String) {
        let newJoinedEvents = joinedEvents + [documentID]
        db.collection("users").document((self.uid)!).updateData([
            "joinedEvents": newJoinedEvents
        ]) { err in
            if let err = err {
                print("addJoinedEvent - Error updating document: \(err)")
            } else {
//                print("addJoinedEvent - Document successfully updated")
            }
        }
        self.collectionView?.reloadData()
    }

    // MARK: location

    private func setupLocationManager() {
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()

        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        currentLocation = CLLocation(latitude: locValue.latitude, longitude: locValue.longitude)
    }

    // MARK: search bar related

    private func setupSearchController() {
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Filter Events"

        navigationItem.searchController = searchController
        definesPresentationContext = true

        searchController.searchBar.scopeButtonTitles = ["All", "5km", "20km", "200km"]
        searchController.searchBar.delegate = self
    }

    func searchBarIsEmpty() -> Bool {
        // Returns true if the text is empty or nil
        return searchController.searchBar.text?.isEmpty ?? true
    }

    func isFiltering() -> Bool {
        let searchBarScopeIsFiltering = searchController.searchBar.selectedScopeButtonIndex != 0
        return searchController.isActive && (!searchBarIsEmpty() || searchBarScopeIsFiltering)
    }

    func filterContentForSearchText(_ searchText: String, scope: String = "All") {
        filteredEventLists = eventLists.filter({(event: Event) -> Bool in

            var isEventNearby: Bool {
                if let currentLocation: CLLocation = self.currentLocation, let eventLocation = event.location {
                    switch scope {
                    case "200km":
                        return eventLocation.distance(from: currentLocation) / 1000 <= 200
                    case "20km":
                        return eventLocation.distance(from: currentLocation) / 1000 <= 20
                    case "5km":
                        return eventLocation.distance(from: currentLocation) / 1000 <= 5
                    case "All":
                        fallthrough
                    default:
                        return true
                    }
                }
                return true
            }

            if searchBarIsEmpty() {
                return isEventNearby
            } else {
                return isEventNearby && event.containsText(searchText.lowercased())
            }
        })

        collectionView?.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
     // MARK: - Navigation

     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using [segue destinationViewController].
     // Pass the selected object to the new view controller.
     }
     */

    // MARK: UICollectionViewDataSource

    //    override func numberOfSections(in collectionView: UICollectionView) -> Int {
    //        // #warning Incomplete implementation, return the number of sections
    //        return 0
    //    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if isFiltering() {
            return filteredEventLists.count
        }

        return eventLists.count
    }

    //    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
    //        return 20
    //    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! ExploreViewCell

        cell.event = isFiltering() ? filteredEventLists[indexPath.item] : eventLists[indexPath.item]
        cell.joined = joinedEvents.contains((cell.event?.documentID)!)
        cell.delegate = self

        // apply cell shadow
        cell.layer.masksToBounds = false
        cell.layer.shadowRadius = 8.0
        cell.layer.shadowColor = UIColor.black.cgColor
        cell.layer.shadowOffset = CGSize(width: 0, height: 3)
        cell.layer.shadowOpacity = 0.1

        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        if filteredEventLists.indices.contains(indexPath.item) || eventLists.indices.contains(indexPath.item) {
            let event = isFiltering() ? filteredEventLists[indexPath.item] : eventLists[indexPath.item]

            let approximateWidthOfDetail = view.frame.width - 12 - 12 - someWidthOffset
            let size = CGSize(width: approximateWidthOfDetail, height: 1000)
            let attributes = [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 14)]

            let estimatedFrame = String(string: event.detail)?.boundingRect(with: size, options: .usesLineFragmentOrigin, attributes: attributes, context: nil)

            return CGSize(width: view.frame.width, height: estimatedFrame!.height + 12 + 24 + 18 + 30 + 12 + someRandomMargin)
        }

        return CGSize(width: view.frame.width, height: 120)
    }

    // MARK: UICollectionViewDelegate

    /*
     // Uncomment this method to specify if the specified item should be highlighted during tracking
     override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
     return true
     }
     */

    /*
     // Uncomment this method to specify if the specified item should be selected
     override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
     return true
     }
     */

    /*
     // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
     override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
     return false
     }

     override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
     return false
     }

     override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {

     }
     */

}

extension ExploreViewController: UISearchResultsUpdating {
    // MARK: - UISearchResultsUpdating Delegate
    func updateSearchResults(for searchController: UISearchController) {
        let searchBar = searchController.searchBar
        let scope = searchBar.scopeButtonTitles![searchBar.selectedScopeButtonIndex]
        filterContentForSearchText(searchController.searchBar.text!, scope: scope)
    }
}

extension ExploreViewController: UISearchBarDelegate {
    // MARK: - UISearchBar Delegate
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        filterContentForSearchText(searchBar.text!, scope: searchBar.scopeButtonTitles![selectedScope])
    }
}

extension Array {
    var last: Element {
        return self[self.endIndex - 1]
    }
}
