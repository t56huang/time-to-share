//
//  ExploreViewCell.swift
//  TimeToShare
//
//  Created by Taoming Huang on 2018-06-21.
//  Copyright © 2018 Taoming Huang. All rights reserved.
//

import UIKit

protocol ExploreViewCellDelegate: class {
    func removeJoinedEvent(documentID: String)
    func addJoinedEvent(documentID: String)
}

class ExploreViewCell: UICollectionViewCell {

    override init(frame: CGRect) {
        super.init(frame: frame)
        joinButton.addTarget(self, action: #selector(tapped), for: .touchUpInside)
        setupViews()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    let titleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 2
        return label
    }()

    let dateLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 2
        return label
    }()

    let detailTextView: UITextView = {
        let textView = UITextView()
        textView.font = UIFont.systemFont(ofSize: 14)
        textView.isScrollEnabled = false
        return textView
    }()

    let joinButton: UIButton = {
        let button = UIButton()

        button.backgroundColor = UIColor(red: 240/255, green: 240/255, blue: 248/255, alpha: 1)

        button.layer.cornerRadius = 13
        button.contentEdgeInsets = UIEdgeInsets(top: 6, left: 16, bottom: 6, right: 16)

        button.setTitleColor(UIColor(red: 0/255, green: 122/255, blue: 255/255, alpha: 1), for: .normal)
        button.titleLabel?.font = .boldSystemFont(ofSize: 14)

        return button
    }()

    weak var delegate: ExploreViewCellDelegate?

    var joined: Bool? {
        didSet {
            if let joined = joined, joined {
//                print("before set title to JOINED")
                joinButton.setTitle("JOINED", for: .normal)

            } else {
//                print("before set title to JOIN")
                joinButton.setTitle("JOIN", for: .normal)
            }
        }
    }

    @objc private func tapped(sender: UIButton!) {
        // UIButton animation
        UIView.animate(withDuration: 0.4, animations: {
            sender.backgroundColor = UIColor(red: 216/255, green: 216/255, blue: 223/255, alpha: 1)
        }, completion: { completed in
            if completed {
                UIView.animate(withDuration: 0.4, animations: {
                    sender.backgroundColor = UIColor(red: 240/255, green: 240/255, blue: 248/255, alpha: 1)
                })

                // update user list
                if let joined = self.joined, joined {
                    // remove documentID from joined list
                    self.delegate?.removeJoinedEvent(documentID: (self.event?.documentID)!)
                } else {
                    // add documentID to joined list
                    self.delegate?.addJoinedEvent(documentID: (self.event?.documentID)!)
                }
            }
        })
    }

    private func setupViews() {
        backgroundColor = UIColor.white

        addSubview(titleLabel)
        addSubview(dateLabel)
        addSubview(detailTextView)
        addSubview(joinButton)

        addConstraintsWithFormat(format: "H:|-12-[v0]-12-[v1]", views: dateLabel, titleLabel)
        addConstraintsWithFormat(format: "H:|-12-[v0]-12-|", views: detailTextView)
        addConstraintsWithFormat(format: "H:[v0]-12-|", views: joinButton)
        addConstraintsWithFormat(format: "V:|-12-[v0][v1]", views: dateLabel, detailTextView)
        addConstraintsWithFormat(format: "V:[v0]-12-|", views: joinButton)
        addConstraintsWithFormat(format: "V:|-12-[v0(40)]", views: titleLabel)
    }

    var event: Event? {
        didSet {
            if let title = event?.title, let time = event?.time, let location = event?.city, let username = event?.username {

                let attributedText = NSMutableAttributedString(string: title, attributes: [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 16)])

                attributedText.append(NSAttributedString(string: "\n\(time) • \(location) • \(username)", attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 14), NSAttributedStringKey.foregroundColor:
                    UIColor.darkGray]))

                titleLabel.attributedText = attributedText
            }

            if let month = event?.month, let date = event?.date  {
                let attributedText = NSMutableAttributedString(string: "\(month)", attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 18), NSAttributedStringKey.foregroundColor:
                    UIColor(red: 250/255, green: 62/255, blue: 62/255, alpha: 1)])
                attributedText.append(NSAttributedString(string: "\n\(date)", attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 24)]))

                dateLabel.attributedText = attributedText
            }

            if let detail = event?.detail {
                detailTextView.text = "\(detail)"
            }
        }
    }
}

extension UIView {
    func addConstraintsWithFormat(format: String, views: UIView...) {
        var viewsDictionary = [String: UIView]()
        for (index, view) in views.enumerated() {
            let key = "v\(index)"
            viewsDictionary[key] = view
            view.translatesAutoresizingMaskIntoConstraints = false
        }

        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format,
                                                      options: NSLayoutFormatOptions(),
                                                      metrics: nil,
                                                      views: viewsDictionary))
    }
}
