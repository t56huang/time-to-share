//
//  FriendListController.swift
//  TimeToShare
//
//  Created by Brad Wang on 2018-06-24.
//  Copyright © 2018 Taoming Huang. All rights reserved.
//

import UIKit
import Firebase
import FirebaseFirestore
import FirebaseAuth

protocol InviteFriendDelegate {
    func setFriend(friends:[String])
}

class InviteFriendListController: UITableViewController{
    
    fileprivate var uid: String!
    fileprivate var db: Firestore!
    
    @IBOutlet var friendListView: UITableView!
    
    var delegate : InviteFriendDelegate?
    
    fileprivate var friendList = [String: String]()
    fileprivate var invitedFriendList = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        db = Firestore.firestore()
        uid = (Auth.auth().currentUser?.uid)
        
        //self.navigationController?.navigationBar.topItem?.title = "Friends List"
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel",
                                                                style: .plain,
                                                                target: self,
                                                                action: #selector(cancelTapped))
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Done",
                                                                 style: .plain,
                                                                 target: self,
                                                                 action: #selector(doneTapped))
        
        
        db.collection("users").document(uid).getDocument {(doc, error) in
            if (error != nil) {
                print("Error retreiving snapshots \(error!)")
                return
            }
            //print("friends are \(doc?.data()!["friends"])")
            self.friendList = doc?.data()!["friends"] as! [String: String]
            self.friendListView.reloadData()
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return friendList.count;
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FriendCell", for: indexPath) as! CheckableTableViewCell
        cell.textLabel?.text = Array(friendList.values)[indexPath.row]
        return (cell)
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        invitedFriendList.append(Array(friendList.keys)[indexPath.row])
    }
    
    override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let deselectedRow = tableView.cellForRow(at: indexPath)
        if invitedFriendList.contains(Array(friendList.keys)[indexPath.row]){
            invitedFriendList.remove(at: indexPath.row)
        }
    }
    
    @objc func cancelTapped(sender: AnyObject) {
        self.navigationController?.navigationBar.topItem?.title = "New Event"
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func doneTapped(sender: AnyObject) {
        self.delegate?.setFriend(friends: self.invitedFriendList)
        self.navigationController?.popViewController(animated: true)
    }
}
