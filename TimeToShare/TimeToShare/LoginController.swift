//
//  LoginController.swift
//  TimeToShare
//
//  Created by Taoming Huang on 2018-06-16.
//  Copyright © 2018 Taoming Huang. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

class LoginController: UIViewController {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
        super.touchesBegan(touches, with: event)
    }

    override func viewDidLayoutSubviews() {
        addTextFieldBottmBorder(textfield: emailTextField)
        addTextFieldBottmBorder(textfield: passwordTextField)
    }

    @IBAction func didTapLoginButton(_ sender: UIButton) {
        // TODO: login with email:kanyewest@example.com and password: kanye
        Auth.auth().signIn(withEmail: emailTextField.text!,
                           password: passwordTextField.text!) {(user, error) in
            if Auth.auth().currentUser != nil {
                //print("Kanye West signed in")
                self.performSegue(withIdentifier:"LoginToTabBar", sender: nil)
            } else {
//                print("No user is signed")
                let alert = UIAlertController(title: "Error",
                                              message: "Incorrect email or password",
                                              preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Cancel",
                                              style: UIAlertActionStyle.default,
                                              handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }

    func addTextFieldBottmBorder(textfield: UITextField) {
        let border = CALayer()
        let width = CGFloat(1.0)
        border.borderColor = UIColor.black.cgColor
        border.frame = CGRect(x: 0,
                              y: textfield.frame.size.height - width,
                              width: textfield.frame.size.width,
                              height: textfield.frame.size.height)
        border.borderWidth = width
        textfield.layer.addSublayer(border)
        textfield.layer.masksToBounds = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
