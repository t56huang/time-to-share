//
//  TabBarController.swift
//  TimeToShare
//
//  Created by Taoming Huang on 2018-06-18.
//  Copyright © 2018 Taoming Huang. All rights reserved.
//

import UIKit
import SwiftIconFont

class TabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Calendar tab
        tabBar.items![0].image =  UIImage(from: .ionicon,
                                          code: "ios-calendar",
                                          textColor: .black,
                                          backgroundColor: .clear,
                                          size: CGSize(width: 35, height: 35))
        tabBar.items![0].title = "Calendar"

        // Friends tab
        tabBar.items![1].image =  UIImage(from: .ionicon,
                                          code: "ios-heart",
                                          textColor: .black,
                                          backgroundColor: .clear,
                                          size: CGSize(width: 35, height: 35))
        tabBar.items![1].title = "Friends"

        // Explore tab
        tabBar.items![2].image =  UIImage(from: .ionicon,
                                          code: "ios-compass",
                                          textColor: .black,
                                          backgroundColor: .clear,
                                          size: CGSize(width: 35, height: 35))
        tabBar.items![2].title = "Explore"

        // Profile tab
        tabBar.items![3].image =  UIImage(from: .ionicon,
                                          code: "ios-person",
                                          textColor: .black,
                                          backgroundColor: .clear,
                                          size: CGSize(width: 35, height: 35))
        tabBar.items![3].title = "Profile"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
