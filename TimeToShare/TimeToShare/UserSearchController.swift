//
//  UserSearchController.swift
//  TimeToShare
//
//  Created by Brad Wang on 2018-07-16.
//  Copyright © 2018 Taoming Huang. All rights reserved.
//

import UIKit
import Firebase

class UserSearchController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var uid: String!
    var uname: String!
    var db: Firestore!
    
    @IBOutlet weak var SearchResult: UITableView!
    
    var userList = [String: String]()
    var userDisplayList = [String: String]()
    var friendList : [String]!
    
    let searchController = UISearchController(searchResultsController: nil)

    override func viewDidLoad() {
        super.viewDidLoad()
        SearchResult.delegate = self
        SearchResult.dataSource = self

        setupSearchController()

        db.collection("users").getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                for document in querySnapshot!.documents {
                    //print("\(document.documentID) => \(document.data())")
                    let data: [String: Any] = document.data()
                    let name: String = data["username"] as! String
                    self.userList[document.documentID] = name
                }
            }
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if #available(iOS 11.0, *) {
            navigationItem.hidesSearchBarWhenScrolling = false
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if #available(iOS 11.0, *) {
            navigationItem.hidesSearchBarWhenScrolling = true
        }
    }
    
    private func setupSearchController() {
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false

        navigationItem.searchController = searchController
        definesPresentationContext = true
    }

    func searchUser(_ searchText: String) {
        let text: String = searchText.lowercased()
//        print("Search Triggered: " + text)
        userDisplayList.removeAll()
        for (id, name) in userList {
            if (id != uid && name.lowercased().range(of: text) != nil) {
                userDisplayList[id] = name
            }
        }
        SearchResult.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func isFriend(name: String) -> Bool {
        for friendname in self.friendList {
            if (name == friendname) {
                return true
            }
        }
        return false
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userDisplayList.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        let name = Array(userDisplayList.values)[indexPath.row] as String
        cell.textLabel?.text = name
        
        if !self.isFriend(name: name) {
            let accept = UIButton()
            accept.tag = indexPath.row
            accept.setTitle("Add Friend", for: .normal)
            accept.setTitleColor(self.view.tintColor, for: .normal)
            accept.frame = CGRect(origin: CGPoint(x: self.view.frame.size.width-111, y: 7), size: CGSize(width: 100, height: 30))
            accept.addTarget(self, action:#selector(ButtonClick(sender:)), for: .touchUpInside)
            cell.addSubview(accept)
        }
        return cell
    }
    
    @objc func ButtonClick(sender: UIButton!) {
        sender.isEnabled = false
        sender.alpha = 0.2;

        print("Send Friend Request at " + String(sender.tag))
        let friendid : String = Array(userDisplayList.keys)[sender.tag]
        
        db.collection("users").document(friendid).getDocument { (document, error) in
            if let document = document, document.exists {
                //let dataDescription = document.data().map(String.init(describing:)) ?? "nil"
                
                var inboxlist : [[String: String]]
                if document.data()!["inbox"] != nil {
                   inboxlist = document.data()!["inbox"] as! [[String: String]]
                }
                else {
                    inboxlist = [[String: String]]()
                }
                var inbox = [String: String]()
                inbox["invite"] = "false"
                inbox["name"] = self.uname
                inbox["userID"] = self.uid
                inboxlist.append(inbox)
                self.db.collection("users").document(friendid).updateData([
                    "inbox": inboxlist,
                    "inboxupdated": true
                ]) { err in
                    if let err = err {
                        print("Error updating friend's inbox: \(err)")
                    } else {
                        print("Friend's inbox successfully updated")
                    }
                }

            } else {
                print("User does not exist")
            }
        }
    }
}

extension UserSearchController: UISearchResultsUpdating {
    // MARK: - UISearchResultsUpdating Delegate
    func updateSearchResults(for searchController: UISearchController) {
        searchUser(searchController.searchBar.text!)
    }
}
