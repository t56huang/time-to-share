//
//  InboxController.swift
//  TimeToShare
//
//  Created by Brad Wang on 2018-07-05.
//  Copyright © 2018 Taoming Huang. All rights reserved.
//

import UIKit
import Firebase


protocol AddFriendDelegate {
    func acceptFriendTapped(value: [String: String])
    func acceptInviteTapped(value: String)
}

class InboxController: UITableViewController {
    
    var delegate : AddFriendDelegate?
    
    var uid: String!
    var db: Firestore!
    
    var inboxList = [[String: String]]()
    var buttons = [Int: (UIButton, UIButton)]()
    var inboxMap = [Int: [String: String]]()
    
    @IBOutlet var inboxView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.inboxView.reloadData()
        //self.listenForInbox()
        
        self.db.collection("users").document(self.uid).updateData([
            "inboxupdated": false
        ]) { err in
            if let err = err {
                print("Error updating inboxupdated: \(err)")
            } else {
                print("inboxupdated successfully updated")
            }
        }
        
        for (i, data) in inboxList.enumerated() {
            inboxMap[i] = data
        }

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return inboxList.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RequestCell", for: indexPath)
        for view in cell.subviews {
            if ((view.accessibilityLabel != nil) && (view.accessibilityLabel != "")) {
                view.removeFromSuperview()
            }
        }

        let label = UILabel()
        label.frame = CGRect(origin: CGPoint(x: 5,y :5), size: CGSize(width: self.view.frame.size.width, height: 20))
        
        let invite = self.inboxList[indexPath.row]["invite"];
        let name = self.inboxList[indexPath.row]["name"];
        if invite == "false" {
            //cell.textLabel?.text = name! + " send you friend request"
            label.text = name! + " send you friend request"
        } else if invite == "true"{
            let eventname = self.inboxList[indexPath.row]["eventname"]
            //cell.textLabel?.text = name! + " invite you to " + eventname!
            label.text = name! + " invite you to " + eventname!
        }
        label.accessibilityLabel = "L"
        cell.addSubview(label)
        
        let accept = UIButton()
        accept.tag = indexPath.row
        accept.setTitle("Accept", for: .normal)
        accept.accessibilityLabel = "A"
        accept.setTitleColor(self.view.tintColor, for: .normal)
        accept.frame = CGRect(origin: CGPoint(x: self.view.frame.size.width-85,y :30), size: CGSize(width: 80, height: 30))
        accept.addTarget(self, action:#selector(ButtonClick(sender:)), for: .touchUpInside)
        cell.addSubview(accept)
        
        let decline = UIButton()
        decline.tag = indexPath.row
        decline.setTitle("Decline", for: .normal)
        decline.accessibilityLabel = "D"
        decline.setTitleColor(UIColor.red, for: .normal)
        decline.frame = CGRect(origin: CGPoint(x: self.view.frame.size.width-160,y :30), size: CGSize(width: 80, height: 30))
        decline.addTarget(self, action:#selector(ButtonClick(sender:)), for: .touchUpInside)
        cell.addSubview(decline)
        
        buttons[indexPath.row] = (accept, decline)
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    @objc func ButtonClick(sender: UIButton!) {
        
        let (accept, decline) = self.buttons[sender.tag]!
        accept.isEnabled = false
        accept.alpha = 0.2;
        decline.isEnabled = false
        decline.alpha = 0.2;
        if (sender.accessibilityLabel! == "A") {
            print("Accept " + String(sender.tag));
            let invite = self.inboxList[sender.tag]["invite"];
            if invite == "false" {
                self.delegate?.acceptFriendTapped(value: self.inboxList[sender.tag])
            } else {
                self.delegate?.acceptInviteTapped(value: self.inboxList[sender.tag]["eventID"]!)
            }
        }
        
        updateInboxList(index: sender.tag)
        /*
        self.inboxView.beginUpdates()
        // update the table view cell
        self.inboxList.remove(at: sender.tag)
        self.updateInboxList()
        self.inboxView.deleteRows(at: [IndexPath(item: sender.tag, section: 0)], with: .automatic)
        //deleteEventIndexPath = nil
        self.inboxView.endUpdates()
        */
    }
    
    private func updateInboxList(index: Int) {
        print("Remove Inbox At " + String(index));
        inboxMap.removeValue(forKey: index)
        db.collection("users").document(uid).updateData([
            "inbox": Array(inboxMap.values)
        ]) { err in
            if let err = err {
                print("Error updating inbox: \(err)")
            } else {
                print("Inbox successfully updated")
            }
        }
    }
/*
    private func listenForInbox() {
        db.collection("users")
            .addSnapshotListener { querySnapshot, error in
                guard let snapshot = querySnapshot else {
                    print("Error retreiving snapshots \(error!)")
                    return
                }
                snapshot.documentChanges.forEach { diff in
                    if (diff.type == .modified && diff.document.documentID == self.uid) {
                        if diff.document.data()["inbox"] != nil {
                            self.inboxList = diff.document.data()["inbox"] as! [[String: String]]
                        } else {
                            self.inboxList = [[String: String]]()
                        }
                    }
                }
        }
    }
*/


    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension UIView {
    func getViewsByTag(tag:Int) -> Array<UIView?>{
        return subviews.filter { ($0 as UIView).tag == tag } as [UIView]
    }
}
