//
//  CreateEventController.swift
//  TimeToShare
//
//  Created by Taoming Huang on 2018-06-11.
//  Copyright © 2018 Taoming Huang. All rights reserved.
//
import UIKit
import Eureka
import CoreLocation
import UserNotifications
import Firebase

protocol CreateEventControllerDelegate {
    func saveTapped(value: [String: Any])
}

class CreateEventController: FormViewController, LocationRowDelegate, InviteFriendDelegate {
    
    var db: Firestore!
    var uid: String!
    
    // for saving data
    var delegate : CreateEventControllerDelegate?

    var selectedDate = Date()
    var locationOb = Location()
    var invitedFriends = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let center = UNUserNotificationCenter.current()
        center.requestAuthorization(options: [.alert, .sound])
            { (granted, error) in
                if !granted {
                    print("user notification access get failed")
                }
        }

        animateScroll = true
        uid = (Auth.auth().currentUser?.uid)
        db = Firestore.firestore()
        initializeForm()
        self.navigationController?.navigationBar.topItem?.title = "New Event"
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel",
                                                                style: .plain,
                                                                target: self,
                                                                action: #selector(cancelTapped))
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Save",
                                                                 style: .plain,
                                                                 target: self,
                                                                 action: #selector(saveTapped))

        rowKeyboardSpacing = 20
    }

    @objc func cancelTapped(sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }

    @objc func saveTapped(sender: AnyObject) {
        let formValues = self.form.values()
        var dict = [String: Any]()
        for (tag, value) in formValues {
            dict[tag] = "\(value ?? "")"
        }
        dict["location"] = locationOb.getFields()
        dict["friends"] = invitedFriends
        print("invited friends are \(dict["friends"])")
        // schedule a notification accroding to user setting
        let notificationCenter = UNUserNotificationCenter.current()
        // MARK: get user notification access
        
        notificationCenter.getNotificationSettings { (settings) in
            guard settings.authorizationStatus == .authorized else {return}
            let content = UNMutableNotificationContent()
            content.title = "Don't forget"
            content.body = dict["title"] as! String
            content.sound = UNNotificationSound.default()
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ssZ"
            dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
            if let date = dateFormatter.date(from: dict["starts"] as! String) {
                if let startDate = Calendar.current.date(byAdding: .minute, value: -15, to: date) {
                    let triggerDate = Calendar.current.dateComponents([.year,.month,.day,.hour,.minute,.second,], from: startDate)
                    
                    // create the trigger
                    let trigger = UNCalendarNotificationTrigger(dateMatching: triggerDate,
                                                                repeats: false)
                    let identifier = "UYLLocalNotification"
                    let request = UNNotificationRequest(identifier: identifier,
                                                        content: content, trigger: trigger)
                    
                    // remove previously scheduled notifications
                    notificationCenter.removeDeliveredNotifications(withIdentifiers: [identifier])
                    
                    // schedule the notification
                    notificationCenter.add(request, withCompletionHandler: { (error) in
                        if let error = error {
                            // Something went wrong
                            print("add notification faield")
                        }
                    })
                    let snoozeAction = UNNotificationAction(identifier: "Snooze",
                                                            title: "Snooze", options: [])
                    let deleteAction = UNNotificationAction(identifier: "UYLDeleteAction",
                                                            title: "Delete", options: [.destructive])
                    let category = UNNotificationCategory(identifier: "UYLReminderCategory",
                                                          actions: [snoozeAction,deleteAction],
                                                          intentIdentifiers: [], options: [])
                    notificationCenter.setNotificationCategories([category])
                    content.categoryIdentifier = "UYLReminderCategory"
                }
            }else{
                print("date cast failed")
            }
        }
        
        self.delegate?.saveTapped(value: dict)
        self.dismiss(animated: true, completion: nil)
    }

    func getLocationCoordinate(degree: CLLocationCoordinate2D) {
        print("mapviewcontroller delegate run")
        locationOb.updateLocationDegree(degree: degree)
        locationOb.getUserCity{success in
            guard success else{return}
            print(self.locationOb.city)
        }
    }
    
    func setFriend(friends: [String]) {
        self.invitedFriends = friends
    }
    
    private func initializeForm() {
        
        form +++

            TextRow("title").cellSetup { cell, row in
                row.title = "Title"
                cell.textField.placeholder = row.title
                row.add(rule: RuleRequired())
                }.cellUpdate{ cell, row in
                    if !row.isValid{
                        cell.titleLabel?.textColor = .red
                    }
                    
            }

            <<< LocationRow("location").cellSetup { cell, row in
                    row.title = "Location"
                    row.locationDelegate = self
                }
            
            +++
            
            SwitchRow("public") {
                $0.title = "Public"
                }.onChange { [weak self] row in
                    
            }

            +++
            
            SwitchRow("visible") {
                $0.title = "Visible to Friends"
                }.onChange { [weak self] row in
                    
            }
            
            +++

            SwitchRow("all-day") {
                $0.title = "All-Day"
                }.onChange { [weak self] row in
                    let startDate: DateTimeInlineRow! = self?.form.rowBy(tag: "starts")
                    let endDate: DateTimeInlineRow! = self?.form.rowBy(tag: "ends")

                    if row.value ?? false {
                        startDate.dateFormatter?.dateStyle = .medium
                        startDate.dateFormatter?.timeStyle = .none
                        endDate.dateFormatter?.dateStyle = .medium
                        endDate.dateFormatter?.timeStyle = .none
                    }
                    else {
                        startDate.dateFormatter?.dateStyle = .short
                        startDate.dateFormatter?.timeStyle = .short
                        endDate.dateFormatter?.dateStyle = .short
                        endDate.dateFormatter?.timeStyle = .short
                    }
                    startDate.updateCell()
                    endDate.updateCell()
                    startDate.inlineRow?.updateCell()
                    endDate.inlineRow?.updateCell()
            }

            <<< DateTimeInlineRow("starts") {
                $0.title = "Starts"
                $0.value = selectedDate.addingTimeInterval(60*60)
                }
                .onChange { [weak self] row in
                    let endRow: DateTimeInlineRow! = self?.form.rowBy(tag: "ends")
                    if row.value?.compare(endRow.value!) == .orderedDescending {
                        endRow.value = Date(timeInterval: 60*60*24, since: row.value!)
                        endRow.cell!.backgroundColor = .white
                        endRow.updateCell()
                    }
                }
                .onExpandInlineRow { [weak self] cell, row, inlineRow in
                    inlineRow.cellUpdate() { cell, row in
                        let allRow: SwitchRow! = self?.form.rowBy(tag: "all-day")
                        if allRow.value ?? false {
                            cell.datePicker.datePickerMode = .date
                        }
                        else {
                            cell.datePicker.datePickerMode = .dateAndTime
                        }
                    }
                    let color = cell.detailTextLabel?.textColor
                    row.onCollapseInlineRow { cell, _, _ in
                        cell.detailTextLabel?.textColor = color
                    }
                    cell.detailTextLabel?.textColor = cell.tintColor
            }

            <<< DateTimeInlineRow("ends"){
                $0.title = "Ends"
                $0.value = selectedDate.addingTimeInterval(60*60*2)
                }
                .onChange { [weak self] row in
                    let startRow: DateTimeInlineRow! = self?.form.rowBy(tag: "starts")
                    if row.value?.compare(startRow.value!) == .orderedAscending {
                        row.cell!.backgroundColor = .red
                    }
                    else{
                        row.cell!.backgroundColor = .white
                    }
                    row.updateCell()
                }
                .onExpandInlineRow { [weak self] cell, row, inlineRow in
                    inlineRow.cellUpdate { cell, dateRow in
                        let allRow: SwitchRow! = self?.form.rowBy(tag: "all-day")
                        if allRow.value ?? false {
                            cell.datePicker.datePickerMode = .date
                        }
                        else {
                            cell.datePicker.datePickerMode = .dateAndTime
                        }
                    }
                    let color = cell.detailTextLabel?.textColor
                    row.onCollapseInlineRow { cell, _, _ in
                        cell.detailTextLabel?.textColor = color
                    }
                    cell.detailTextLabel?.textColor = cell.tintColor
        }

        form +++

            PushRow<EventState>("show-as") {
                $0.title = "Show As"
                $0.options = EventState.allValues
        }
        
        form +++
            ButtonRow("invite"){
                $0.title = "Invite Friends"
                $0.onCellSelection(self.buttonTapped)
        }

        form +++
            TextAreaRow("notes") {
                $0.placeholder = "Notes"
                $0.textAreaHeight = .dynamic(initialTextViewHeight: 50)
        }
    }

    enum EventState : String, CustomStringConvertible {
        case busy
        case free
        var description : String { return rawValue }
        static let allValues = [busy, free]
        
    }
    
    func buttonTapped(cell: ButtonCellOf<String>, row: ButtonRow) {
        db.collection("users").document(uid).getDocument {(doc, error) in
            if (error != nil) {
                print("Error retreiving snapshots \(error!)")
                return
            }
            if doc?.data()!["friends"] == nil {
                let alert = UIAlertController(title: "Please add a friend", message: "You have NO friend !!!", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Got it", style: .default, handler: nil))
                self.present(alert, animated: true)
            }else{
                let inviteFriendController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "InviteFriendController") as! InviteFriendListController
                inviteFriendController.delegate = self
                self.navigationController?.pushViewController(inviteFriendController, animated: true)
            }
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
