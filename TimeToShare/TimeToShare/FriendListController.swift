//
//  FriendListController.swift
//  TimeToShare
//
//  Created by Brad Wang on 2018-06-24.
//  Copyright © 2018 Taoming Huang. All rights reserved.
//

import UIKit
import Firebase
import FirebaseFirestore
import FirebaseAuth

class FriendListController: UITableViewController, AddFriendDelegate{
    
    fileprivate var uid: String!
    fileprivate var uname: String!
    fileprivate var db: Firestore!
    
    @IBOutlet weak var InboxButton: UIBarButtonItem!
    
    @IBOutlet var friendListView: UITableView!
    
    fileprivate var friendList = [String: String]()
    fileprivate var inboxList = [[String: String]]()
    fileprivate var joinedList = [String]()
    fileprivate var inboxupdated = false

    override func viewDidLoad() {
        super.viewDidLoad()
        db = Firestore.firestore()
        uid = (Auth.auth().currentUser?.uid)

        // use large titles
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationController?.navigationBar.topItem?.title = "Friend List"

        db.collection("users").document(uid).getDocument {(doc, error) in
            if (error != nil) {
                print("Error retreiving snapshots \(error!)")
                return
            }
            var data = doc?.data()
            if data!["friends"] != nil {
                self.friendList = data!["friends"] as! [String: String]
            } else {
                self.friendList = [String: String]()
            }
            if data!["inbox"] != nil {
                self.inboxList = data!["inbox"] as! [[String: String]]
            } else {
                self.inboxList = [[String: String]]()
            }
            if data!["joinedEvents"] != nil {
                self.joinedList = data!["joinedEvents"] as! [String]
            } else {
                self.joinedList = [String]()
            }
            if data!["inboxupdated"] != nil {
                self.inboxupdated = data!["inboxupdated"] as! Bool
            } else {
                self.inboxupdated = false
            }
            
            if self.inboxupdated {
                print("Update Badge \(self.inboxList.count)")
                self.InboxButton.addBadge(number: self.inboxList.count)
            } else {
                self.InboxButton.removeBadge()
            }
            
            self.uname = data!["username"] as! String
            self.friendListView.reloadData()
        }
        
        self.listenForFriends()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return friendList.count;
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FriendCell", for: indexPath)
        cell.textLabel?.text = Array(friendList.values)[indexPath.row]
        return (cell)
    }
    
    // MARK: select the table cell
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //self.performSegue(withIdentifier: "ViewFriendCalendar", sender: indexPath.row)
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    func acceptFriendTapped(value: [String: String]) {
        let friendID: String! = value["userID"]
        let friendName: String! = value["name"]
        db.collection("users").document(uid).updateData([
            ("friends."+friendID): friendName
        ]) { err in
            if let err = err {
                print("Error updating friend: \(err)")
            } else {
                print("Friends successfully updated")
                
                self.db.collection("users").document(friendID).updateData([
                    ("friends."+self.uid): self.uname
                ]) { err in
                    if let err = err {
                        print("Error updating friend: \(err)")
                    } else {
                        print("Friends successfully updated")
                        
                        
                    }
                }
            }
        }
    }
    
    func acceptInviteTapped(value: String) {
        self.joinedList.append(value)
        db.collection("users").document(uid).updateData([
            "joinedEvents": self.joinedList
        ]) { err in
            if let err = err {
                print("Error updating joined events: \(err)")
            } else {
                print("Joined events successfully updated")
            }
        }
    }
    
    private func listenForFriends() {
        db.collection("users")
            .addSnapshotListener { querySnapshot, error in
                guard let snapshot = querySnapshot else {
                    print("Error retreiving snapshots \(error!)")
                    return
                }
                
                snapshot.documentChanges.forEach { diff in
                    if (diff.type == .modified && diff.document.documentID == self.uid) {
                        var data = diff.document.data()
                        if data["friends"] != nil {
                            self.friendList = data["friends"] as! [String: String]
                        } else {
                            self.friendList = [String: String]()
                        }
                        if data["inbox"] != nil {
                            self.inboxList = data["inbox"] as! [[String: String]]
                        } else {
                            self.inboxList = [[String: String]]()
                        }
                        if data["joinedEvents"] != nil {
                            self.joinedList = data["joinedEvents"] as! [String]
                        } else {
                            self.joinedList = [String]()
                        }
                        if data["inboxupdated"] != nil {
                            self.inboxupdated = data["inboxupdated"] as! Bool
                        } else {
                            self.inboxupdated = false
                        }
                        if self.inboxupdated {
                            print("Update Badge \(self.inboxList.count)")
                            self.InboxButton.addBadge(number: self.inboxList.count)
                        } else {
                            self.InboxButton.removeBadge()
                        }
                    }
                }
                self.friendListView.reloadData()
        }
    }


    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        
        if segue.identifier == "ShowInboxSegue"
        {
            let c = segue.destination as! InboxController
            c.uid = self.uid
            c.db = self.db
            c.inboxList = self.inboxList
            c.delegate = self
        }
        else if segue.identifier == "ShowSearchSegue" {
            let c = segue.destination as! UserSearchController
            c.uname = self.uname
            c.uid = self.uid
            c.db = self.db
            c.friendList = Array(friendList.values)
        }
        else if segue.identifier == "ViewFriendCalendar" {
            let navVC = segue.destination as? UINavigationController
            let c = navVC?.viewControllers.first as! FriendCalendarController
            c.db = self.db
            let indexPath = friendListView.indexPath(for: sender as! UITableViewCell)
            c.uid = Array(friendList.keys)[(indexPath?.row)!]
            c.username = Array(friendList.values)[(indexPath?.row)!]
        }
    }

}
