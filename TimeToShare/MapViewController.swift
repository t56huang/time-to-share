//
//  MapViewController.swift
//  TimeToShare
//
//  Created by Qiaoge Zheng on 2018-07-07.
//  Copyright © 2018 Taoming Huang. All rights reserved.
//

import Foundation
import UIKit
import MapKit
import Eureka
import Firebase
import FirebaseFirestore
import FirebaseAuth
import CoreLocation

protocol MapViewControllerDelegate {
    func getLocationCoordinate(degree: CLLocationCoordinate2D)
}


public class MapViewController : UIViewController, TypedRowControllerType, MKMapViewDelegate,CLLocationManagerDelegate {
    
    public var row: RowOf<CLLocation>!
    public var onDismissCallback: ((UIViewController) -> ())?
    
    var delegate : MapViewControllerDelegate?
    var locationManager = CLLocationManager()
    
    var droppedPin : CGPoint?
    
    lazy var mapView : MKMapView = { [unowned self] in
        let v = MKMapView(frame: self.view.bounds)
        v.autoresizingMask = UIViewAutoresizing.flexibleWidth.union(.flexibleHeight)
        return v
        }()
    
    let width: CGFloat = 10.0
    let height: CGFloat = 5.0
    
    lazy var ellipse: UIBezierPath = { [unowned self] in
        let ellipse = UIBezierPath(ovalIn: CGRect(x: 0, y: 0, width: self.width, height: self.height))
        return ellipse
        }()
    
    
    lazy var ellipsisLayer: CAShapeLayer = { [unowned self] in
        let layer = CAShapeLayer()
        layer.bounds = CGRect(x: 0, y: 0, width: self.width, height: self.height)
        layer.path = self.ellipse.cgPath
        layer.fillColor = UIColor.gray.cgColor
        layer.fillRule = kCAFillRuleNonZero
        layer.lineCap = kCALineCapButt
        layer.lineDashPattern = nil
        layer.lineDashPhase = 0.0
        layer.lineJoin = kCALineJoinMiter
        layer.lineWidth = 1.0
        layer.miterLimit = 10.0
        layer.strokeColor = UIColor.gray.cgColor
        return layer
        }()
    
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    public override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nil, bundle: nil)
    }
    
    convenience public init(_ callback: ((UIViewController) -> ())?){
        self.init(nibName: nil, bundle: nil)
        onDismissCallback = callback
    }
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(mapView)
        
        mapView.delegate = self
        
        let button = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done, target: self, action: #selector(self.tappedDone(_:)))
        button.title = "Done"
        navigationItem.rightBarButtonItem = button
        
        let longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(self.addAnnotationOnLongPress(_:)))
        longPressGesture.minimumPressDuration = 1.0
        self.mapView.addGestureRecognizer(longPressGesture)
        
        mapView.showsUserLocation = true
        if CLLocationManager.locationServicesEnabled() == true {
            if CLLocationManager.authorizationStatus() == .restricted ||
                CLLocationManager.authorizationStatus() == .denied ||
                CLLocationManager.authorizationStatus() == .notDetermined {
                locationManager.requestWhenInUseAuthorization()
            }
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.delegate = self
            locationManager.startUpdatingLocation()
        }else{
            print("please turn on GPS or location services")
        }
        updateTitle()
        
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    //MARK : CLLocation Manager delegates
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: locations[0].coordinate.latitude, longitude: locations[0].coordinate.longitude), span: MKCoordinateSpan(latitudeDelta: 0.2, longitudeDelta: 0.2))
        self.mapView.setRegion(region, animated: true)
    }
    
    public func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Unable to access your current location")
    }
    
    @objc func tappedDone(_ sender: UIBarButtonItem){
        // check if user dropped a pin
        if droppedPin == nil{
            let alert = UIAlertController(title: "Please select a location", message: "Please use long press to select a location", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Got it", style: .default, handler: nil))            
            self.present(alert, animated: true)
            return
        }
        
        let target = mapView.convert(ellipsisLayer.position, toCoordinateFrom: mapView)
        row.value = CLLocation(latitude: target.latitude, longitude: target.longitude)
        let degrees = CLLocationCoordinate2D(latitude: target.latitude, longitude: target.longitude)
        //print(" map view delegate is \(self.delegate)")
        self.delegate?.getLocationCoordinate(degree: degrees)
        onDismissCallback?(self)
    }
    
    @objc func addAnnotationOnLongPress(_ sender: UILongPressGestureRecognizer){
        droppedPin = sender.location(in: self.mapView)
        let point = sender.location(in: self.mapView)
        let coordinate = self.mapView.convert(point, toCoordinateFrom: self.mapView)
        //print(coordinate)
        
        //Now use this coordinate to add annotation on map.
        var annotation = MKPointAnnotation()
        annotation.coordinate = coordinate
        //Set title and subtitle if you want
        annotation.title = "Title"
        annotation.subtitle = "subtitle"
        
        self.mapView.removeAnnotations(mapView.annotations)
        self.mapView.addAnnotation(annotation)

    }
    
    func updateTitle(){
        let fmt = NumberFormatter()
        fmt.maximumFractionDigits = 4
        fmt.minimumFractionDigits = 4
        let latitude :CLLocationDegrees = self.mapView.centerCoordinate.latitude
        let longitude :CLLocationDegrees = self.mapView.centerCoordinate.longitude
        let location = CLLocation(latitude: latitude, longitude: longitude)
        //print(location)
        // calculate the city from longi and lati
        CLGeocoder().reverseGeocodeLocation(location, completionHandler: {(placemarks, error) -> Void in
            //print(location)
            
            if error != nil {
                print("Reverse geocoder failed with error" + (error?.localizedDescription)!)
                return
            }
            
            if (placemarks?.count)! > 0 {
                let pm = placemarks![0]
                //print(pm.locality as Any)
                if let streetNumber = pm.subThoroughfare, let street = pm.thoroughfare {
                    self.title = "\(streetNumber), \(street)"
                }else{
                    self.title = "Please drop a pin"
                    print("city cast faield")
                }
            }
            else {
                print("Problem with the data received from geocoder")
            }
        })
        
    }
    
    public func mapView(_ mapView: MKMapView, regionWillChangeAnimated animated: Bool) {
        ellipsisLayer.transform = CATransform3DMakeScale(0.5, 0.5, 1)
//        UIView.animate(withDuration: 0.2, animations: { [weak self] in
//            self?.pinView.center = CGPoint(x: self!.pinView.center.x, y: self!.pinView.center.y - 10)
//        })
    }
    
    public func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        ellipsisLayer.transform = CATransform3DIdentity
//        UIView.animate(withDuration: 0.2, animations: { [weak self] in
//            self?.pinView.center = CGPoint(x: self!.pinView.center.x, y: self!.pinView.center.y + 10)
//        })
        updateTitle()
    }
}
